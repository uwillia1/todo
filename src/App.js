import React, { Component } from "react";
import "./App.css";
import SingleTodo from "./SingleTodo";

class App extends Component {
  constructor() {
    super();
    this.state = {
      todos: [],
      currentTodo: ""
    };
  }

  onInputChange = (event) => {
    this.setState({ currentTodo: event.target.value });
  };

  addTodo = () => {
    let todosCopy = this.state.todos.slice();
    todosCopy.push(this.state.currentTodo);

    this.setState({ todos: todosCopy, currentTodo: "" });
  };

  deleteTodo = (i) => {
    let todosCopy = this.state.todos.slice();
    todosCopy.splice(i, 1);
    this.setState({ todos: todosCopy });
  };

  render() {
    let bulletedTodos = this.state.todos.map((element, i) => {
      return (
        <SingleTodo
          todo={element}
          delete={() => {
            this.deleteTodo(i);
          }}
        ></SingleTodo>
      );
    });
    return (
      <div>
        <input
          placeholder="Enter todo"
          value={this.state.currentTodo}
          onChange={this.onInputChange}
        />
        <button onClick={this.addTodo}>Add</button>
        <br></br>
        {this.state.todos.length === 0 ? (
          "Nothing yet!"
        ) : (
          <ul>{bulletedTodos}</ul>
        )}
      </div>
    );
  }
}

export default App;
